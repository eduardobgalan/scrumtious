﻿using Restaurant_App.Interfaces;
using Restaurant_App.ViewModels;
using Restaurant_App.Views.Order;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using Xamarin.Forms;

namespace Restaurant_App.Views.Actions
{
    public partial class Message : ContentPage, INotifyPropertyChanged
    {
        Api_PlaceViewModel placeOrders = new Api_PlaceViewModel();
        public ObservableCollection<Models.Message> Messages { get; set; }

        public Message(Api_PlaceViewModel model)
        {
            InitializeComponent();
            placeOrders = model;

            Messages = new ObservableCollection<Models.Message>();
            DataButton.IsEnabled = false;

            /*especificar cuanto va a tardar en preguntar*/
            Device.StartTimer(TimeSpan.FromSeconds(10), () =>
            {
                CallBack_Messages();
                return true;
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "CHAT";
            this.BackgroundColor = Color.White;

            CallBack_Messages();
        }

        async void SendData(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            var currentUser = App.CurrentUser();

            var binding = (Models.Order)this.BindingContext;

            var model = new Models.Message()
            {
                Content = DataText.Text,
                Position = LayoutOptions.End,
                User = new Models.User() { Name = currentUser.u_n },
                Date = DateTime.Now.ToString("HH:mm:ss"),
                Txt_IsVisible = !string.IsNullOrEmpty(DataText.Text)
            };

            var messageCreated = await App.Manager.SetOrderMessage(new Models.Message()
            {
                Content = DataText.Text,
                OrderId = ((Models.Order)this.BindingContext).OrderId,
                UserId = currentUser.u_id,
                ImageBtm = binding.ImageBtm
            });

            if (messageCreated != null)
            {
                if (!string.IsNullOrEmpty(messageCreated.Image)) // Realiza estas acciones ÚNICAMENTE si el mensaje tiene imagen.
                {
                    model.Image = messageCreated.Image;
                    model.Img_IsVisible = true;
                    model.ImgWidth = 100;
                }

                Messages.Add(model);

                ListaRecursos.ItemsSource = Messages;
                ListaRecursos.ItemSelected += On_ItemSelected;
                ListaRecursos.ScrollTo(model, ScrollToPosition.MakeVisible, true);

                DataText.Text = "";
                binding.ImageBtm = null;
            }
            else
            {
                await DisplayAlert("Oops", "Message not delivered.", "Close");
                DataButton.IsEnabled = false;
            }
        }

        async void On_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var binding = (Models.Message)e.SelectedItem;

            if (!string.IsNullOrEmpty(binding.Image))
            {
                var destination = new View_Image();
                destination.BindingContext = binding.Image;

                await Navigation.PushModalAsync(destination, true);
            }
        }

        private void Entry1_TextChanged(object sender, TextChangedEventArgs e)
        {
            var entry = (Entry)sender;

            DataButton.IsEnabled = (entry.Text.Length > 0);
        }

        /// <summary>
        /// Consulta el servicio de los mensajes.
        /// </summary>
        async void CallBack_Messages()
        {
            var messages = await App.Manager.GetOrderMessages(((Models.Order)this.BindingContext).OrderId);

            if (messages != null)
            {
                if (messages.Count != Messages.Count) // Si el servicio tiene mas mensajes que los actuales.
                {
                    for (var i = Messages.Count; i <= messages.Count - 1; i++)
                    {
                        var _msg = messages[i];

                        _msg.Position = _msg.UserId == App.CurrentUser().u_id ? LayoutOptions.End : LayoutOptions.Start;
                        _msg.ImgWidth = _msg.Image != null ? 100 : 0;
                        _msg.Img_IsVisible = _msg.Image != null;
                        _msg.Txt_IsVisible = !string.IsNullOrEmpty(_msg.Content);
                        _msg.Date = _msg.CreationDate.Date == DateTime.Now.Date ? _msg.CreationDate.ToString("HH:mm:ss") : _msg.CreationDate.ToString("MM/dd/yyyy");
                        Messages.Add(_msg);
                    }

                    ListaRecursos.ItemsSource = Messages;
                    ListaRecursos.ItemSelected += On_ItemSelected;
                    ListaRecursos.ScrollTo(messages[messages.Count - 1], ScrollToPosition.MakeVisible, true);
                }
            }
        }

        void On_Back(object sender, EventArgs e)
        {
            var model = (Models.Order)this.BindingContext;

            var destination = new OrderDetail(model);
            destination.BindingContext = placeOrders;
            Application.Current.MainPage = new MainPage(destination);
        }

        async void On_TakePicture(object sender, EventArgs e)
        {
            Stream stream = await DependencyService.Get<IPicturePicker>().GetImageStreamAsync();

            if (stream != null)
            {
                var imageSrc = ImageSource.FromStream(() => stream);
                var binding = ((Models.Order)this.BindingContext);

                binding.ImageBtm = ReadFully(stream);
                DataButton.IsEnabled = true;
                //Photo_Msg.Text = "Photo chosen";
            }
        }

        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
