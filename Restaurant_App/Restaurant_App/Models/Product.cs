﻿using Restaurant_App.Enums;
using System;
using System.Collections.Generic;

namespace Restaurant_App.Models
{
    public class Product
    {
        public Int32 ProductId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String Image { get; set; }
        public String SKU { get; set; }
        public ProductStatus Status { get; set; }
        public Decimal Price { get; set; }
        public DateTime CreationDate { get; set; }
        public Boolean IsDeleted { get; set; }
        public Int32 CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public List<Unit> Units { get; set; }
    }
}
