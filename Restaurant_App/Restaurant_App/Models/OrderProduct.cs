﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant_App.Models
{
    public class OrderProduct
    {
        public Int32 OrderProductId { get; set; }
        public Decimal Price { get; set; }
        public Int32 Quantity { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime SealDate { get; set; }
        public Boolean IsDeleted { get; set; }
        public Int32 ProductId { get; set; }
        public virtual Product Product { get; set; }
        public Int32 OrderId { get; set; }
        public virtual Order Oder { get; set; }
        public Decimal Total { get; set; }
        public Int32 UnitId { get; set; }
    }
}
