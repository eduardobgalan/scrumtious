﻿using Restaurant_App.Enums;
using System;
using System.Collections.Generic;

namespace Restaurant_App.Models
{
    public class Order
    {
        public Int32 OrderId { get; set; }
        public String Concept { get; set; }
        public String Token { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? DeliverDate { get; set; }
        public OrderPriority Priority { get; set; }
        public String PriorityDescription { get; set; }
        public OrderStatus Status { get; set; }
        public String StatusColor { get; set; }
        public String StatusDescription { get; set; }
        public Int32 PlaceId { get; set; }
        public virtual Place Place { get; set; }
        public Boolean IsDeleted { get; set; }
        public DateTime? DesiredDate { get; set; }
        public List<OrderProduct> OrderProducts { get; set; }
        public Decimal Total { get; set; }
        public byte[] ImageBtm { get; set; }
        public String ColorShape { get; set; }
        public String ShapeText { get; set; }
    }
}
