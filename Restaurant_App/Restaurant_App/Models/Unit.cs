﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant_App.Models
{
    public class Unit
    {
        public Int32 UnitId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Decimal MaxWeight { get; set; }
        public Boolean IsDeleted { get; set; }
    }
}
