﻿using System;

namespace Restaurant_App.Models
{
    public class Report
    {
        public Int32 ReportId { get; set; }
        public Int32 OrderId { get; set; }
        public virtual Order Order { get; set; }
        public String Message { get; set; }
        public String Image { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
