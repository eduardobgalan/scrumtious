﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Restaurant_App.Models
{
    public class Category : INotifyPropertyChanged
    {
        public Int32 CategoryId { get; set; }
        public String Name { get; set; }
        public String Image { get; set; }
        public Boolean IsProductCategory { get; set; }
        public Boolean IsDeleted { get; set; }
        public Boolean IsSelected { get; set; }
        public Boolean IsVisible { get; set; }
        public List<Product> Products { get; set; }
        public string SelectedCount { get; set; }

        private String Img { get; set; }
        public String RadioImage
        {
            get
            {
                return Img;
            }

            set
            {
                if (value != Img)
                {
                    Img = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
