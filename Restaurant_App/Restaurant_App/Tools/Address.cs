﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Restaurant_App.Tools
{
    /// <summary>
    /// Herramienta para obtener una dirección en base a las coordenadas de entrada.
    /// </summary>
    public class GeoCode
    {
        /// <summary>
        /// Latitud a buscar.
        /// </summary>
        public string Latitude { get; set; }
        /// <summary>
        /// Longitud a buscar.
        /// </summary>
        public string Longitude { get; set; }

        private const string _gMapsKey = "AIzaSyDHDAgflTW7I5-0DTeCuMvo2NKeK2flo0A";

        /// <summary>
        /// Constructor público sin parámetros.
        /// </summary>
        public GeoCode() { }

        /// <summary>
        /// Función que busca la dirección.
        /// </summary>
        /// <returns></returns>
        public string GetAddress()
        {
            HttpClient client = new HttpClient();
            var addrs = new Results();

            var uri = string.Format("https://maps.googleapis.com/maps/api/geocode/json?latlng={0},{1}&key={2}", this.Latitude, this.Longitude, _gMapsKey); ;

            try
            {
                var response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;

                    addrs = JsonConvert.DeserializeObject<Results>(content);
                }

                return addrs.results[0].formatted_address;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }

    public class Results
    {
        public List<Address> results { get; set; }
    }

    public class Address
    {
        public string formatted_address { get; set; }
    }
}
