﻿using Newtonsoft.Json;
using Restaurant_App.Classes;
using Restaurant_App.Views;
using Restaurant_App.Views.Help;
using Restaurant_App.Views.Order;
using Restaurant_App.Views.Place;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Restaurant_App
{
	public partial class MainPage : MasterDetailPage
	{
        public List<MasterPageItem> menuList { get; set; }

        public MainPage(Page detailpage)
        {
            InitializeComponent();

            this.BackgroundColor = Color.White;

            menuList = new List<MasterPageItem> {
               new MasterPageItem() { Title = "My places", Icon = "Content/Icono_MyPlaces.png", TargetType = typeof(MyPlaces) },
               new MasterPageItem() { Title = "New order", Icon = "Content/Icono_NewOrder.png", TargetType = typeof(AddProducts) },
               new MasterPageItem() { Title = "FAQ", Icon = "Content/Icono_Faq.png", TargetType = typeof(FAQ) },
               //new MasterPageItem() { Title = "No orders", Icon = "itemIcon9.png", TargetType = typeof(NoOrder) },
               new MasterPageItem() { Title = "Sign out", Icon = "Content/Icono_SignOut.png", TargetType = typeof(Login) }
           };

            // Mandamos los datos a la vista
            navigationDrawerList.ItemsSource = menuList;
            // Codigo que se usa para que el menu de hamburguesa aparezca en windows.

            if (detailpage != null)
            {
                Detail = new NavigationPage(detailpage);
            }
            else
            {
                var destination = new NoPlace();
                Detail = new NavigationPage(destination);
            }

            //  Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(TestPage1)));

            if (Device.RuntimePlatform == Device.Windows)
            {
                MasterBehavior = MasterBehavior.Popover;
            }
        }

        // Eventos al hacer click en cada elmento del menu.
        private void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = (MasterPageItem)e.SelectedItem;
            Type page = item.TargetType;            

            if (page == typeof(Login))
            {
                Application.Current.Properties.Clear();
                Detail = new NavigationPage(new Login());
            } else if (page == typeof(MyPlaces))
            {
                var usr_cred = App.CurrentUser();
                
                Detail = new NavigationPage(new MyPlaces(usr_cred.u_id));
            } else
            {
                Detail = new NavigationPage((Page)Activator.CreateInstance(page));
            }
            
            //Este valor siempre debe estar false, para que no se quede pegado el elemento del menu
            IsPresented = false;
        }
    }
}
