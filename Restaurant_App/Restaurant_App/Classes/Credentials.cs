﻿namespace Restaurant_App.Classes
{
    public static class Credentials
    {
        private static string host = "http://restauranteweb.comulink.net/";
        //private static string host = "http://localhost:6794/";

        //api routes
        #region Login module
        public static string Login = host + "api/users/login_async";
        public static string CreateAccount = host + "api/users/register_async";
        public static string RecoverPassword = host + "api/users/password_recover_async";
        #endregion Login module

        #region Places module
        public static string ManagePlace = host + "api/places/manage";
        public static string GetPlaces = host + "api/places/get_with_orders/{0}";
        public static string GetCategories = host + "api/categories/get";
        #endregion Places module

        #region Orders module
        public static string CreateOder = host + "api/orders/manage";
        public static string GetProducts = host + "api/products/get";
        public static string GetOrderById = host + "";
        public static string GetOrdersByPlaceId = host + "";
        public static string ChangeOrderStatus = host + "api/orders/change_status";
        public static string SetOrderMessage = host + "api/messages/manage";
        public static string GetOrderMessages = host + "api/messages/get_by_order";
        public static string SetOrderReport = host + "api/messages/manage";
        #endregion

        // resources
        //public static string ImageUpload = host + "api/image/upload";
    }
}
