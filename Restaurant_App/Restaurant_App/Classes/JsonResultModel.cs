﻿namespace Restaurant_App.Classes
{
    public class JsonResultModel
    {
        public dynamic OK { set; get; }
        public dynamic Model { set; get; }
        public dynamic Message { set; get; }
    }
}
