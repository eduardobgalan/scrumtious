﻿namespace Restaurant_App.Classes
{
    public class User_Credentials
    {
        /// <summary>
        /// UserId del usuario actual.
        /// </summary>
        public int u_id { get; set; }
        /// <summary>
        /// Expiration date de las credenciales actuales.
        /// </summary>
        public string ex_da { get; set; }
        /// <summary>
        /// GUID de las credenciales.
        /// </summary>
        public string x_ma { get; set; }
        /// <summary>
        /// Token de usuario.
        /// </summary>
        public string u_cd { get; set; }
        /// <summary>
        /// Nombre de usuario
        /// </summary>
        public string u_n { get; set; }
    }
}
