﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Restaurant_App.Classes
{
    public static class EnumDescription
    {
        public static string GetEnumDescription(this Enum enumVal)
        {
            try
            {
                return enumVal.GetType()
                    .GetMember(enumVal.ToString())
                    .First()
                    .GetCustomAttribute<DescriptionAttribute>()
                    .Description;
            }
            catch
            {
                return "";
            }
        }
    }
}
