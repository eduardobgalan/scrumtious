﻿using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using Restaurant_App.Classes;
using Restaurant_App.Interfaces;
using Restaurant_App.Services;
using Restaurant_App.Views;
using Restaurant_App.Views.Place;
using Xamarin.Forms;

namespace Restaurant_App
{
    public partial class App : Application
    {
        public static Manager Manager { get; private set; }
        public static ISpeech Speech { get; set; }

        public App()
        {
            InitializeComponent();

            Manager = new Manager(new GeneralService());

            var usr_cred = CurrentUser();

            if (usr_cred != null)
            {
                var destination = new MyPlaces(usr_cred.u_id);
                Application.Current.MainPage = new MainPage(destination);
            } else
            {
                MainPage = new Login();
            }
        }
        
        protected override void OnStart()
        {
            // Handle when your app starts
            AppCenter.Start("ios=7655d005-f53e-4ffa-8ca5-4fb801f948fa;" + "uwp={Your UWP App secret here};" +
                   "android={Your Android App secret here}",
                   typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        /// <summary>
        /// Regresa las credenciales del usuario actual.
        /// </summary>
        /// <returns>Objeto User_Credentials</returns>
        public static User_Credentials CurrentUser()
        {
            try
            {
                var obj = Application.Current.Properties["user_credentials"].ToString();
                return JsonConvert.DeserializeObject<User_Credentials>(obj);
            }
            catch
            {
                return null;
            }
        }
    }
}
