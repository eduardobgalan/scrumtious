﻿using Newtonsoft.Json;
using Restaurant_App.Models.Payment;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Restaurant_App.Views.Payment
{
	public partial class CreatePay : ContentPage
	{
		public CreatePay ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "PAYMENT METHOD";
            this.BackgroundColor = Color.White;
            this.BindingContext = new Charge();
        }

        public string CreateToken(string cardNumber, int cardExpMonth, int cardExpYear, string cardCVC)
        {
            StripeConfiguration.SetApiKey("pk_test_j7QB0p8up0njZigyjwSXGGIM");

            var tokenOptions = new StripeTokenCreateOptions
            {
                Card = new StripeCreditCardOptions
                {
                    Number = cardNumber,
                    ExpirationYear = cardExpYear,
                    ExpirationMonth = cardExpMonth,
                    Cvc = cardCVC
                }
            };

            try
            {
                var tokenService = new StripeTokenService();
                StripeToken stripeToken = tokenService.Create(tokenOptions);

                return stripeToken.Id;
            }
            catch
            {
                return null;
            }
        }

        async void On_Pay(object sender, EventArgs e)
        {
            var model = (Charge)this.BindingContext;

            var date = model.ExpirationDate.Split('/');

            var token = CreateToken(model.Number, Convert.ToInt32(date[0]), Convert.ToInt32(date[1]), model.Cvc);

            if (token != null)
            {
                HttpClient client = new HttpClient();
                var response = await client.PostAsync("http://restauranteweb.comulink.net/api/stripe/create_charge",
                                       new StringContent(JsonConvert.SerializeObject(new PaymentModel()
                                       {
                                           Amount = model.Amount,
                                           Token = token
                                       }),
                                       Encoding.UTF8, "application/json"));
            } else
            {
                await DisplayAlert("Ops", "Error trying to connect with the server.", "Close");
            }
        }
    }
}
