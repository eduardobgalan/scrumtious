﻿using Newtonsoft.Json;
using Restaurant_App.Classes;
using Restaurant_App.Enums;
using Restaurant_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Restaurant_App.Views.Order
{
    public partial class CreateOrder : ContentPage
    {
        List<Api_PlaceViewModel> places = new List<Api_PlaceViewModel>();
        Dictionary<int, string> getPlaces = new Dictionary<int, string>();
        Dictionary<int, string> getPriority = new Dictionary<int, string>();

        public CreateOrder()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            this.BackgroundColor = Color.White;

            var usr_cred = App.CurrentUser();

            places = await App.Manager.GetPlaces(usr_cred.u_id);
            getPlaces = places.ToDictionary(i => i.Place.PlaceId, i => string.IsNullOrEmpty(i.Place.Name) ? "Place: " + i.Place.PlaceId.ToString() : i.Place.Name);

            //Place.Items.Add("- Select -"); // Creamos la opcion 'Select'
            foreach (var item in getPlaces)
            {
                Place.Items.Add(item.Value);
            }
            //Place.SelectedIndex = 0; // Seleccionamos el primer elemento

            getPriority = Enum.GetValues(typeof(OrderPriority)).Cast<OrderPriority>().ToDictionary(t => (int)t, t => EnumDescription.GetEnumDescription(t));

            //Priority.Items.Add("- Select -"); // Creamos la opcion 'Select'
            foreach (var item in getPriority)
            {
                Priority.Items.Add(item.Value);
            }
            //Priority.SelectedIndex = 0; // Seleccionamos el primer elemento

            var model = (OrderViewModel)this.BindingContext;

            model.Order.CreationDate = DateTime.Now;
            model.Order.DesiredDate = DateTime.Now;

            if (model.Order.Place != null)
                Place.SelectedItem = model.Order.Place.Name; // Seleccionamos el elemento Place.

            try
            {
                Priority.SelectedItem = EnumDescription.GetEnumDescription(model.Order.Priority); // Seleccionamos el elemento Priority.
            }
            catch
            {
                Priority.SelectedItem = null;
            }

            this.BindingContext = model;
        }

        async void On_Save(object sender, EventArgs e)
        {
            var model = (OrderViewModel)this.BindingContext;

            if (Place.SelectedIndex >= 0 && Priority.SelectedIndex >= 0)
            {
                var element = places[Place.SelectedIndex];
                model.Order.PlaceId = element.Place.PlaceId;
                model.Order.Priority = (OrderPriority)Enum.Parse(typeof(OrderPriority), Priority.SelectedItem.ToString());

                var destination = new ConfirmOrder();
                destination.BindingContext = model;
                Application.Current.MainPage = new MainPage(destination);
            }
            else
            {
                await DisplayAlert("Ops", "You have to select a place and a priority.", "Acept");
            }
        }

        void On_Change(object sender, EventArgs e)
        {
            var item = (Picker)sender;

            if (item.SelectedItem.ToString() == "- Select -" || string.IsNullOrEmpty(item.SelectedIndex.ToString()))
            {
                item.SelectedIndex = 0;
                return;
            }

            var model = (OrderViewModel)BindingContext;
            var prior = OrderPriority.High;
            try
            {
                prior = (OrderPriority)Enum.Parse(typeof(OrderPriority), item.SelectedItem.ToString());
            }
            catch
            {
                prior = OrderPriority.High;
            }

            switch (prior)
            {
                case OrderPriority.Low: model.Order.DesiredDate = model.Order.CreationDate.AddDays(2); break;
                case OrderPriority.Medium: model.Order.DesiredDate = model.Order.CreationDate.AddDays(1); break;
                case OrderPriority.High: model.Order.DesiredDate = model.Order.CreationDate.AddDays(0); break;
                default: model.Order.DesiredDate = model.Order.CreationDate.AddDays(0); break;
            }

            this.BindingContext = null;
            this.BindingContext = model;
        }

        void On_Back(object sender, EventArgs e)
        {
            var destination = new SelectCategory();
            destination.BindingContext = BindingContext;
            destination.Title = "CATEGORIES";
            Application.Current.MainPage = new MainPage(destination);
        }
    }
}
