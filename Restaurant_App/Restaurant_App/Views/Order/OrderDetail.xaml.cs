﻿using Newtonsoft.Json;
using Restaurant_App.Classes;
using Restaurant_App.Models;
using Restaurant_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Restaurant_App.Views.Order
{
	public partial class OrderDetail : ContentPage
	{
        List<OrderProduct> products = new List<OrderProduct>();
        Models.Order order = new Models.Order();
        Api_PlaceViewModel placeOrders = new Api_PlaceViewModel();

        public OrderDetail (Models.Order model)
		{
			InitializeComponent ();
            order = model;
            if (order.OrderProducts != null)
            {
                foreach (var item in order.OrderProducts)
                {
                    item.Total = item.Quantity * item.Price;
                }
            }
            order.Total = order.OrderProducts.Sum(x => x.Total);
            products = order.OrderProducts;
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "MY ORDER DETAIL";
            this.BackgroundColor = Color.White;

            placeOrders = (Api_PlaceViewModel)this.BindingContext;
            this.BindingContext = null;
            this.BindingContext = order;
            if (order.Status == Enums.OrderStatus.Completed)
            {
                Received.IsEnabled = false;
            } else
            {
                Received.IsEnabled = true;
            }

            switch (order.Status)
            {
                case Enums.OrderStatus.Pending:
                    LabelStatus.TextColor = Color.FromHex("#e8353d");
                    LabelStatus.Text = "PENDING";
                    ImageStatus.Source = "Content/Icono_Pending.png";
                    break;
                case Enums.OrderStatus.InTransit:
                    LabelStatus.TextColor = Color.FromHex("#fbbd6e");
                    LabelStatus.Text = "ON THE GO";
                    ImageStatus.Source = "Content/Icono_OnTheGo.png";
                    break;
                case Enums.OrderStatus.Completed:
                    LabelStatus.TextColor = Color.FromHex("#b9c65c");
                    LabelStatus.Text = "COMPLETED";
                    ImageStatus.Source = "Content/Icono_Completed.png";
                    break;
            }

            var subtotal = order.Total;
            var taxes = subtotal * (decimal)0.16;
            var total = subtotal + taxes;

            Subtotal.Text = subtotal.ToString("C");
            Taxes.Text = taxes.ToString("C");
            Total.Text = total.ToString("C");
            ProductsList.ItemsSource = products;
        }

        async void On_Received(object sender, EventArgs e)
        {
            try
            {
                var btn = (Button)sender;
                var content = (Models.Order)btn.BindingContext;
                var order = new Models.Order
                {
                    OrderId = content.OrderId,
                    Status = Enums.OrderStatus.Completed
                };
                var changed = await App.Manager.ChangeOrderStatus(order);
                if (changed)
                {
                    Received.IsEnabled = false;
                    await DisplayAlert("Great", "We're glad you received your order. Thanks for your trust.", "Close");
                }
                else
                {
                    await DisplayAlert("Sorry", "We couldn't change your status as received. Please contact support.", "Close");
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Sorry", "We couldn't change your status as received. Please contact support.", "Close");
            }
        }

        void On_Claim(object sender, EventArgs e)
        {
            var destination = new Actions.Report(placeOrders);
            destination.BindingContext = this.BindingContext;
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_Chat(object sender, EventArgs e)
        {
            var destination = new Actions.Message(placeOrders);
            destination.BindingContext = this.BindingContext;
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_Back(object sender, EventArgs e)
        {
            var destination = new PlaceOrders();
            destination.BindingContext = placeOrders;
            Application.Current.MainPage = new MainPage(destination);
        }
    }
}
