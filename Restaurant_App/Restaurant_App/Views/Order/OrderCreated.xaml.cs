﻿using Newtonsoft.Json;
using Restaurant_App.Classes;
using Restaurant_App.Views.Place;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Restaurant_App.Views.Order
{
	public partial class OrderCreated : ContentPage
	{
		public OrderCreated ()
		{
			InitializeComponent ();

            this.BackgroundColor = Color.White;
		}
        
        void On_Done(object sender, EventArgs e)
        {
            var usr_cred = App.CurrentUser();

            var destination = new MyPlaces(usr_cred.u_id);
            Application.Current.MainPage = new MainPage(destination);
        }
    }
}
