﻿using Restaurant_App.Models;
using Restaurant_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Restaurant_App.Views.Order
{
    public partial class ConfirmOrder : ContentPage
    {
        OrderViewModel order = new OrderViewModel();

        public ConfirmOrder()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "REVIEW MY ORDER";
            this.BackgroundColor = Color.White;

            order = (OrderViewModel)this.BindingContext;

            var subtotal = order.Products.Sum(x => x.Total);
            var taxes = subtotal * (decimal)0.16;
            var total = subtotal + taxes;

            Subtotal.Text = subtotal.ToString("C");
            Taxes.Text = taxes.ToString("C");
            Total.Text = total.ToString("C");

            ProductsList.ItemsSource = order.Order.OrderProducts;
        }

        async void Place_Order(object sender, EventArgs e)
        {
            var model = (OrderViewModel)this.BindingContext;

            await App.Manager.CreateOrder(model.Order);

            var destination = new OrderCreated();
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_Remove(object sender, EventArgs e)
        {
            var btn = (Button)sender;
            var obj = (OrderProduct)btn.BindingContext;
            order.Order.OrderProducts.Remove(obj);
            ProductsList.ItemsSource = null;
            ProductsList.ItemsSource = order.Order.OrderProducts;

            var subtotal = order.Order.OrderProducts.Sum(x => x.Total);
            var taxes = subtotal * (decimal)0.16;
            var total = subtotal + taxes;

            Subtotal.Text = subtotal.ToString("C");
            Taxes.Text = taxes.ToString("C");
            Total.Text = total.ToString("C");
        }

        void On_Add(object sender, EventArgs e)
        {
            var model = (OrderViewModel)this.BindingContext;

            var destination = new SelectCategory();
            destination.BindingContext = model;
            Application.Current.MainPage = new MainPage(destination);
        }
    }
}
