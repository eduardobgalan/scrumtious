﻿using Restaurant_App.Models;
using Restaurant_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Restaurant_App.Views.Order
{
    public partial class SelectCategory : ContentPage
    {
        public SelectCategory()
        {
            InitializeComponent();
        }

        public ICollection<Category> Categories { get; set; }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            Title = "CATEGORIES";
            BackgroundColor = Color.White;

            Categories = await App.Manager.GetCategories();
            Categories.ToList().ForEach(x => Category_SetCount(x, x.Products));
            BindingContext_Build(); // Construye el binding actual.

            List_Categories.ItemsSource = Categories;
        }

        void On_Item_Selected(object sender, SelectedItemChangedEventArgs e)
        {
            var destination = new AddProducts();

            var selectedProducts = ((Category)e.SelectedItem).Products;

            var currentProducts = ((OrderViewModel)BindingContext).Products;
            currentProducts.ForEach(x => Product_Hide(x));

            foreach (var item in selectedProducts)
            {
                var product = currentProducts.SingleOrDefault(x => x.Product.ProductId == item.ProductId);

                if (product != null)
                    product.IsVisible = true;
            }

            destination.BindingContext = BindingContext;
            destination.Title = "PRODUCTS";
            Application.Current.MainPage = new MainPage(destination);
        }

        async void On_Next(object sender, EventArgs e)
        {
            var model = (OrderViewModel)this.BindingContext;
            var products = model.Products.Where(x => x.Qty > 0).ToList();
            List<OrderProduct> OP = new List<OrderProduct>();

            model.Order = new Models.Order()
            {
                CreationDate = DateTime.Now,
                DesiredDate = DateTime.Now,
            };

            if (products.Count > 0)
            {
                foreach (var item in products)
                {
                    item.Total = item.Qty * item.Product.Price;
                    var unitId = item.Product.Units;

                    var content = new OrderProduct
                    {
                        ProductId = item.Product.ProductId,
                        Product = item.Product,
                        Quantity = item.Qty,
                        Price = item.Product.Price,
                        Total = item.Qty * item.Product.Price
                    };
                    OP.Add(content);
                }

                model.Order.OrderProducts = OP;

                var destination = new CreateOrder();
                destination.BindingContext = model;
                destination.Title = "NEW ORDER";
                Application.Current.MainPage = new MainPage(destination);
            }
            else
            {
                await DisplayAlert("Ops", "You must select at least one product.", "Accept");
            }
        }

        #region Private Methods
        private void Category_SetCount(Category _cat, List<Product> _prods)
        {
            var currentBinding = (OrderViewModel)BindingContext;

            if (currentBinding == null)
                _cat.SelectedCount = "0/" + _prods.Count();
            else
            {
                var selectedCounter = 0;
                foreach (var item in _prods)
                {
                    if (currentBinding.Products.Where(x => x.Qty != 0).SingleOrDefault(x => x.Product.ProductId == item.ProductId) != null)
                        selectedCounter++;
                }

                _cat.SelectedCount = selectedCounter + "/" + _prods.Count();
            }

        }

        private void Product_Hide(ProductViewModel _prod)
        {
            _prod.IsVisible = false;
        }

        private void BindingContext_Build()
        {
            string imageHost = "http://restauranteweb.comulink.net/Content/Images/";
            var products = Categories.SelectMany(x => x.Products);
            var productsVM = new List<ProductViewModel>();

            var currentBinding = (OrderViewModel)BindingContext;
            foreach (var item in products)
            {
                item.Name = item.Name.ToUpper();
                item.Image = imageHost + item.Image;

                if (currentBinding != null)
                {
                    var currentProduct = currentBinding.Products != null ? currentBinding.Products.SingleOrDefault(x => x.Product.ProductId == item.ProductId) : null;

                    var currentOrderProduct = currentBinding.Order != null ? currentBinding.Order.OrderProducts.SingleOrDefault(x => x.ProductId == item.ProductId) : null;

                    if (currentProduct != null)
                        productsVM.Add(currentProduct);
                    else
                    {
                        var model = new ProductViewModel
                        {
                            Product = item,
                            Quantity = 10,
                            IsVisible = false,
                            Qty = currentOrderProduct != null ? currentOrderProduct.Quantity : 0
                        };

                        productsVM.Add(model);
                    }
                }
                else
                {
                    var model = new ProductViewModel
                    {
                        Product = item,
                        Quantity = 10,
                        IsVisible = false,
                    };

                    productsVM.Add(model);
                }
            }

            BindingContext = new OrderViewModel()
            {
                Products = productsVM,
                Order = currentBinding != null ? currentBinding.Order : null,
            };
        }
        #endregion
    }
}