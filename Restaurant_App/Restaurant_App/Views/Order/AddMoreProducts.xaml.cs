﻿using Restaurant_App.Models;
using Restaurant_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Restaurant_App.Views.Order
{
	public partial class AddMoreProducts : ContentPage
	{
        List<Product> products = new List<Product>();
        List<ProductViewModel> productsVM = new List<ProductViewModel>();
        List<Category> categories = new List<Category>();
        Dictionary<int, string> getCategories = new Dictionary<int, string>();
        OrderViewModel order = new OrderViewModel();
        public string imageHost = "http://restauranteweb.comulink.net/Content/Images/";

        public AddMoreProducts ()
		{
			InitializeComponent ();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "CATALOG";
            this.BackgroundColor = Color.White;

            searchBtn.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { On_Filter(); }) });
            clearBtn.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { On_Clear(); }) });

            products = await App.Manager.GetProducts();
            categories = await App.Manager.GetCategories();

            foreach (var item in products)
            {
                item.Name = item.Name.ToUpper();
                item.Image = imageHost + item.Image;
                var model = new ProductViewModel
                {
                    Product = item,
                    Quantity = 10
                };

                productsVM.Add(model);
            }

            getCategories = categories.ToDictionary(i => i.CategoryId, i => i.Name);

            //Category.Items.Add("Category");  // Creamos la opcion 'Category'
            foreach (var item in getCategories)
            {
                Category.Items.Add(item.Value);
            }

            Category.SelectedIndex = 0; // Seleccionamos el primer elemento

            order.Products = productsVM;
            ProductsList.ItemsSource = order.Products;

            var currBinding = (OrderViewModel)this.BindingContext;
            if (currBinding != null)
            {
                var orderToClone = (OrderViewModel)this.BindingContext;

                foreach (var _prod in orderToClone.Order.OrderProducts) // Recorremos los detalles actuales
                {
                    var prod = order.Products.SingleOrDefault(x => x.Product.ProductId == _prod.ProductId); // Buscamos el producto en la lista
                    prod.Qty = _prod.Quantity; // Cambiamos la cantidad
                }

                orderToClone.Order.CreationDate = DateTime.Now;
                orderToClone.Order.DesiredDate = DateTime.Now;

                this.BindingContext = new OrderViewModel()
                {
                    Order = orderToClone.Order, // Orden a clonar
                    Products = order.Products, // Productos ya actualizados
                };
            }
            else
            {
                this.BindingContext = new OrderViewModel()
                {
                    Order = new Models.Order()
                    {
                        CreationDate = DateTime.Now,
                        DesiredDate = DateTime.Now,
                    },
                    Products = order.Products
                };
            }
        }

        void On_Done(object sender, EventArgs e)
        {
            var model = (OrderViewModel)this.BindingContext;
            var products = (List<ProductViewModel>)productsVM.Where(x => x.Qty > 0).ToList();
            List<OrderProduct> OP = new List<OrderProduct>();

            foreach (var item in products)
            {
                item.Total = item.Qty * item.Product.Price;
                var unitId = item.Product.Units;
                var content = new OrderProduct
                {
                    ProductId = item.Product.ProductId,
                    Product = item.Product,
                    Quantity = item.Qty,
                    Price = item.Product.Price,
                    Total = item.Qty * item.Product.Price
                };
                OP.Add(content);
            }
            model.Order.OrderProducts = OP;
            model.Products = products;

            var destination = new ConfirmOrder();
            destination.BindingContext = model;
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_Unit_Selected(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            var unit = (Unit)picker.SelectedItem;

            var product = (ProductViewModel)picker.BindingContext;
            product.UnitId = unit.UnitId;
        }

        void On_Add(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            var model = (ProductViewModel)btn.BindingContext;

            var product = (ProductViewModel)productsVM.SingleOrDefault(x => x.Product.ProductId == model.Product.ProductId);

            if (product.Qty < product.Quantity)
            {
                product.Qty += 1;
            }
        }

        void On_Remove(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            var model = (ProductViewModel)btn.BindingContext;

            var product = (ProductViewModel)productsVM.SingleOrDefault(x => x.Product.ProductId == model.Product.ProductId);

            if (product.Qty > 0)
            {
                product.Qty -= 1;
            }
        }

        void On_Picker_Selected(object sender, EventArgs e)
        {
            Filter.Text = "";
            var cat = categories[Category.SelectedIndex];
            ProductsList.ItemsSource = (List<ProductViewModel>)productsVM.Where(x => x.Product.CategoryId == cat.CategoryId).ToList();
        }

        void On_Filter()
        {
            Category.SelectedIndex = 0;
            var str = Filter.Text;
            ProductsList.ItemsSource = null;
            ProductsList.ItemsSource = (List<ProductViewModel>)productsVM.Where(x => x.Product.Name.Contains(str)).ToList();
        }

        void On_Clear()
        {
            Filter.Text = "";
            Category.SelectedIndex = 0;
            ProductsList.ItemsSource = null;
            ProductsList.ItemsSource = (List<ProductViewModel>)productsVM.ToList();
        }
    }
}
