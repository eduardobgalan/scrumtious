﻿using Newtonsoft.Json;
using Restaurant_App.Classes;
using Restaurant_App.Enums;
using Restaurant_App.ViewModels;
using Restaurant_App.Views.Place;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Restaurant_App.Views.Order
{
    public partial class PlaceOrders : ContentPage
    {
        List<Models.Order> orders = new List<Models.Order>();
        Api_PlaceViewModel placeOrders = new Api_PlaceViewModel();
        private Random rnd = new Random();

        public PlaceOrders()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "MY ORDERS";
            this.BackgroundColor = Color.White;

            var model = (Api_PlaceViewModel)BindingContext;
            placeOrders = model;

            if (model != null)
                orders = model.Place.Orders;

            if (orders != null)
            {
                foreach (var item in orders)
                {
                    //item.ColorShape = String.Format("#{0:X6}", rnd.Next(0x1000000));
                    item.Concept = item.Concept.ToUpper();
                    item.ShapeText = item.Concept.Substring(0, 1).ToUpper();
                    switch (item.Status)
                    {
                        case OrderStatus.New:
                            item.StatusColor = "#6da8cd";
                            item.StatusDescription = EnumDescription.GetEnumDescription(item.Status);
                            break;
                        case OrderStatus.Pending:
                            item.StatusColor = "#e8353d";
                            item.StatusDescription = EnumDescription.GetEnumDescription(item.Status);
                            break;
                        case OrderStatus.InTransit:
                            item.StatusColor = "#fbbd6e";
                            item.StatusDescription = EnumDescription.GetEnumDescription(item.Status);
                            break;
                        case OrderStatus.Completed:
                            item.StatusColor = "#b9c65c";
                            item.StatusDescription = EnumDescription.GetEnumDescription(item.Status);
                            break;
                        case OrderStatus.Error:
                            item.StatusColor = "#ed686e";
                            item.StatusDescription = EnumDescription.GetEnumDescription(item.Status);
                            break;
                    }
                }

                OrdersList.ItemsSource = orders;
            }
            else
            {
                await Task.Run(async () =>
                {
                    await Task.Delay(300);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        var destination = new NoOrder();
                        Application.Current.MainPage = new MainPage(destination);
                    });
                });
            }
        }

        void On_Item_Selected(object sender, SelectedItemChangedEventArgs e)
        {
            var model = (Models.Order)e.SelectedItem;

            var destination = new OrderDetail(model);
            destination.BindingContext = (Api_PlaceViewModel)this.BindingContext;
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_Back(object sender, EventArgs e)
        {
            var obj = Application.Current.Properties["user_credentials"].ToString();
            var usr_cred = JsonConvert.DeserializeObject<User_Credentials>(obj);

            var destination = new MyPlaces(usr_cred.u_id);
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_Add(object sender, EventArgs e)
        {
            var destination = new SelectCategory();
            destination.Title = "CATEGORIES";
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_Clone(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            var model = (Models.Order)btn.BindingContext;
            model.OrderId = 0;

            var destination = new SelectCategory();
            destination.BindingContext = new OrderViewModel()
            {
                Order = model
            };
            destination.Title = "CATALOG";
            Application.Current.MainPage = new MainPage(destination);
        }
    }
}
