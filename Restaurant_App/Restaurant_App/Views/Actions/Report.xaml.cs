﻿using Restaurant_App.Interfaces;
using Restaurant_App.ViewModels;
using Restaurant_App.Views.Order;
using System;
using System.IO;
using Xamarin.Forms;

namespace Restaurant_App.Views.Actions
{
    public partial class Report : ContentPage
    {
        Models.Order model { get; set; }
        Api_PlaceViewModel placeOrders = new Api_PlaceViewModel();

        public Report(Api_PlaceViewModel model)
        {
            InitializeComponent();
            placeOrders = model;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "Report";
            this.BackgroundColor = Color.White;
        }

        async void SendData(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            var currentUser = App.CurrentUser();

            var messageCreated = await App.Manager.SetOrderMessage(new Models.Message()
            {
                Content = Content.Text,
                OrderId = ((Models.Order)this.BindingContext).OrderId,
                UserId = currentUser.u_id,
                IsReport = true,
                ImageBtm = ((Models.Order)this.BindingContext).ImageBtm,
            });

            if (messageCreated != null)
            {
                await DisplayAlert("Done", "Your report has been sent.", "Close");
                Content.Text = "";
                Photo_Msg.Text = "";
            }
            else
                await DisplayAlert("Oops", "Something went wrong. Check your data", "Close");
        }

        void On_Back(object sender, EventArgs e)
        {
            var context = (Models.Order)this.BindingContext;

            var destination = new OrderDetail(context);
            destination.BindingContext = placeOrders;
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_Call(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("tel:016622000201"));
        }

        async void On_TakePicture(object sender, EventArgs e)
        {
            Stream stream = await DependencyService.Get<IPicturePicker>().GetImageStreamAsync();

            if (stream != null)
            {
                var imageSrc = ImageSource.FromStream(() => stream);
                var binding = ((Models.Order)this.BindingContext);

                binding.ImageBtm = ReadFully(stream);
                Photo_Msg.Text = "Photo chosen";
            }
        }

        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
