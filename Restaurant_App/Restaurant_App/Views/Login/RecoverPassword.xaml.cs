﻿using Restaurant_App.ViewModels;
using System;
using Xamarin.Forms;

namespace Restaurant_App.Views
{
    public partial class RecoverPassword : ContentPage
    {
        public RecoverPassword()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.BindingContext = new LoginViewModel();
        }

        void Get_Back(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            Device.BeginInvokeOnMainThread(() => { btn.Text = "Redirecting..."; });

            //var destination = new Login();
            //Application.Current.MainPage = new MainPage(destination);

            Navigation.PopModalAsync();
        }

        async void On_Recover(object sender, EventArgs e)
        {
            var model = (LoginViewModel)BindingContext;

            var usr_recover = await App.Manager.RecoverPassword(model);

            if (usr_recover)
            {
                await Navigation.PopModalAsync();

                //var destination = new Login();
                //Application.Current.MainPage = new MainPage(destination);

                await DisplayAlert("Done", "Check your email inbox", "Close");
            }
            else
                await DisplayAlert("Oops", "something went wrong. Please try again", "Close");
        }
    }
}
