﻿using Newtonsoft.Json;
using Restaurant_App.Classes;
using Restaurant_App.ViewModels;
using Restaurant_App.Views.Place;
using System;
using Xamarin.Forms;

namespace Restaurant_App.Views
{
    public partial class Login : ContentPage
    {
        public Login()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            
            //this.BindingContext = new LoginViewModel();

            this.BindingContext = new LoginViewModel()
            {
                Email = "andreina@comulink.com.mx",
                Password = "Morfeo2017."
            };

            CreateAccountLb.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { Create_Account(); }) });
            ForgotPassWordLabel.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { Forgot_Password(); }) });

        }

        async void Sing_In(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            var model = (LoginViewModel)BindingContext;

            var logged_usr = await App.Manager.Login(model);

            if (logged_usr != null)
            {
                var userCredentials = new User_Credentials()//Generamos el objeto a almacenar en la memoria del dispositivo
                {
                    ex_da = DateTime.Now.AddDays(7).ToString("dd/MM/yyyy"),
                    x_ma = Guid.NewGuid().ToString(),
                    u_cd = logged_usr.Token,
                    u_id = logged_usr.UserId,
                    u_n = logged_usr.Name
                };

                Application.Current.Properties["user_credentials"] = JsonConvert.SerializeObject(userCredentials).ToString();//Lo serializamos

                await Application.Current.SavePropertiesAsync();//Lo guardamos

                var destination = new MyPlaces(userCredentials.u_id);
                Application.Current.MainPage = new MainPage(destination);
            }
            else
            {
                Application.Current.Properties.Remove("user_credentials");//Removemos el elemento del diccionario para evitar problemas
                await DisplayAlert("Oops", "Check your credentials", "Close");
            }
        }

        void Forgot_Password()
        {
            var destination = new RecoverPassword();
            Navigation.PushModalAsync(destination);
            //Application.Current.MainPage = new MainPage(destination);
        }

        void Create_Account()
        {
            var destination = new CreateAccount();
            Navigation.PushModalAsync(destination);
            //Application.Current.MainPage = new MainPage(destination);
        }
    }
}
