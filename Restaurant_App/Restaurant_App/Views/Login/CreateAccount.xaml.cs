﻿using Restaurant_App.Models;
using System;
using Xamarin.Forms;

namespace Restaurant_App.Views
{
    public partial class CreateAccount : ContentPage
    {
        public CreateAccount()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.BindingContext = new User();
            SignInLabel.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { Sing_In(); }) });
        }

        void Sing_In()
        {
            Navigation.PopModalAsync();
            //var destination = new Login();
            //Application.Current.MainPage = new MainPage(destination);
        }

        async void On_Create(object sender, EventArgs e)
        {
            var btn = (Button)sender;

            var model = (User)BindingContext;

            var user_created = await App.Manager.CreateUser(model);

            if (user_created._succ)
                await Navigation.PopModalAsync();
            //var destination = new Login();
            //Application.Current.MainPage = new MainPage(destination);
            else
                await DisplayAlert("Oops", user_created._err.Model, "Close");
        }
    }
}
