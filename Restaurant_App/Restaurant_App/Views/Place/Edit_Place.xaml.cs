﻿using Restaurant_App.Interfaces;
using Restaurant_App.ViewModels;
using Restaurant_App.Views.Actions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Restaurant_App.Views.Place
{
    public partial class Edit_Place : ContentPage
    {
        public Edit_Place()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "EDIT PLACE";
            this.BackgroundColor = Color.White;

            var model = (Models.Place)this.BindingContext;

            Btn_View_Photo.IsVisible = !string.IsNullOrEmpty(model.Image);

            openMap.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { On_OpenMap(); }) });
        }

        void On_Save(object sender, EventArgs e)
        {
            var model = (Models.Place)BindingContext;

            var usr_cred = App.CurrentUser();

            model.UserId = usr_cred.u_id;

            var destination = new NewPlace_2();
            destination.BindingContext = model;
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_OpenMap()
        {
            var destination = new MapEdit();

            destination.Title = "SELECT LOCATION";
            destination.BindingContext = this.BindingContext;

            Application.Current.MainPage = new MainPage(destination);
        }

        void On_ViewPicture(object sender, EventArgs e)
        {
            var destination = new View_Image();
            destination.BindingContext = ((Models.Place)this.BindingContext).Image;

            Navigation.PushModalAsync(destination, true);
        }

        async void On_TakePicture(object sender, EventArgs e)
        {
            Stream stream = await DependencyService.Get<IPicturePicker>().GetImageStreamAsync();

            if (stream != null)
            {
                var imageSrc = ImageSource.FromStream(() => stream); // Esto se asigna al label de imagen (no funciona)
                var binding = (Models.Place)this.BindingContext;

                binding.ImageBtm = ReadFully(stream);
                Photo_Msg.Text = "Image loaded successfully";
                Btn_View_Photo.IsVisible = false;
            }
        }

        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
