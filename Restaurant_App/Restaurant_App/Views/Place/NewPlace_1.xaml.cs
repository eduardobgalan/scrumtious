﻿using Newtonsoft.Json;
using Restaurant_App.Classes;
using Restaurant_App.Interfaces;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using Xamarin.Forms;

namespace Restaurant_App.Views.Place
{
    public partial class NewPlace_1 : ContentPage
    {
        public NewPlace_1()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.Title = "NEW PLACE";
            this.BackgroundColor = Color.White;

            this.BindingContext = (Models.Place)BindingContext ?? new Models.Place();

            openMap.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { On_OpenMap(); }) });
        }

        void On_Save(object sender, EventArgs e)
        {
            var model = (Models.Place)BindingContext;

            var usr_cred = App.CurrentUser();

            model.UserId = usr_cred.u_id;

            var destination = new NewPlace_2();
            destination.BindingContext = model;
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_OpenMap()
        {
            var destination = new MapTest();

            destination.Title = "SELECT LOCATION";
            destination.BindingContext = this.BindingContext;

            Application.Current.MainPage = new MainPage(destination);
        }

        async void On_TakePicture(object sender, EventArgs e)
        {
            Stream stream = await DependencyService.Get<IPicturePicker>().GetImageStreamAsync();

            if (stream != null)
            {
                var imageSrc = ImageSource.FromStream(() => stream);
                var binding = (Models.Place)this.BindingContext;

                binding.ImageBtm = ReadFully(stream);
                Photo_Msg.Text = "Image loaded successfully";
            }
        }

        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
