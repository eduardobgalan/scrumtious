﻿using Restaurant_App.Tools;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace Restaurant_App.Views.Place
{
    public partial class MapTest : ContentPage
    {
        public MapTest()
        {
            InitializeComponent();
        }

        public Map map { get; set; }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            this.BackgroundColor = Color.White;

            var btn = new Button()
            {
                Text = "SET LOCATION",
                BackgroundColor = Color.FromHex("ff4d5d"),
                TextColor = Color.FromHex("ffffff"),
                HeightRequest = 60
            };

            btn.Clicked += On_Sel_Location; // Asigna el evento al boton

            BuildMap();

            var content = new ScrollView
            {
                VerticalOptions = new LayoutOptions(LayoutAlignment.Start, true),
                Content = new StackLayout
                {
                    Children =
                    {
                        map,
                        btn // Boton para guardar.
                    }
                }
            };

            Content = content;
        }

        void On_Back(object sender, EventArgs e)
        {
            var destination = new NewPlace_1();

            Application.Current.MainPage = new MainPage(destination);
        }

        void On_Sel_Location(object sender, EventArgs e)
        {
            var pin = map.Pins[0];

            var binding = (Models.Place)BindingContext;

            binding.Latitude = (decimal)pin.Position.Latitude;
            binding.Longitude = (decimal)pin.Position.Longitude;

            // Inicializamos herramienta de dirección.
            var addr = new GeoCode()
            {
                Latitude = binding.Latitude.ToString(),
                Longitude = binding.Longitude.ToString()
            };

            binding.Address = addr.GetAddress(); // Asignamos el resultado a la dirección.

            var destination = new NewPlace_1();
            destination.BindingContext = binding;
            Application.Current.MainPage = new MainPage(destination);
            // await DisplayAlert("Great", "Now we have your address.", "Close");
        }

        #region Private
        /// <summary>
        /// Construye mapa.
        /// </summary>
        private void BuildMap()
        {
            var pin = new Pin()
            {
                IsDraggable = true,
                Position = new Position(33.451697, -112.070151),
                Type = PinType.Generic,
                Label = "Drag pin to select location",
                Icon = BitmapDescriptorFactory.DefaultMarker(Color.Gray),
                Flat = true,
            };

            map = new Map()
            {
                HeightRequest = 500,
                WidthRequest = 500,
                VerticalOptions = LayoutOptions.FillAndExpand,
                IsEnabled = true,
            };

            map.Pins.Add(pin);
            map.MapClicked += (sender, e) =>
            {
                map.Pins[0].Position = e.Point;
            };

            // Función para centrar el mapa con respecto al pin.
            map.MoveToRegion(MapSpan.FromCenterAndRadius(pin.Position, Distance.FromMiles(150)), false);
        }
        #endregion
    }
}