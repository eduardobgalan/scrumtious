﻿using Restaurant_App.ViewModels;
using Restaurant_App.Views.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Restaurant_App.Views.Place
{
    public partial class MyPlaces : ContentPage
    {
        List<Api_PlaceViewModel> places = new List<Api_PlaceViewModel>();
        public Int32 userId { get; set; }

        public MyPlaces(int id)
        {
            InitializeComponent();

            userId = id;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            Title = "MY PLACES";
            this.BackgroundColor = Color.FromHex("#f7f7f7");

            places = await App.Manager.GetPlaces(userId);

            if (places.Count == 0)
            {
                var destination = new NoPlace();
                Application.Current.MainPage = new MainPage(destination);
            }

            foreach (var item in places)
            {
                item.Place.Name = item.Place.Name != null ? item.Place.Name.ToUpper() : "";
            }

            PlacesList.ItemsSource = places;
        }

        void On_Add(object sender, EventArgs e)
        {
            var destination = new NewPlace_1();
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_Edit(object sender, EventArgs e)
        {
            var item = (MenuItem)sender;

            var model = (Api_PlaceViewModel)item.CommandParameter;
            var binding = (Models.Place)model.Place;

            var destination = new Edit_Place();
            destination.BindingContext = binding;
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var model = e.SelectedItem;

            var destination = new PlaceOrders();
            destination.BindingContext = model;
            Application.Current.MainPage = new MainPage(destination);
        }

        void On_FilterOrders(object sender, EventArgs e)
        {
            var btn = (StackLayout)sender;
            var child = (Label)btn.Children[2];
            var text = child.Text;
            Enums.OrderStatus status = new Enums.OrderStatus();

            switch (text)
            {
                case "Pending":
                    status = Enums.OrderStatus.Pending;
                    break;
                case "In transit":
                    status = Enums.OrderStatus.InTransit;
                    break;
                case "Completed":
                    status = Enums.OrderStatus.Completed;
                    break;
            }

            var binding = (Api_PlaceViewModel)btn.BindingContext;
            var orders = (List<Models.Order>)binding.Place.Orders;
            binding.Place.Orders = orders.Where(x => x.Status == status).ToList();
            var destination = new PlaceOrders();
            destination.BindingContext = binding;

            Application.Current.MainPage = new MainPage(destination);
        }
    }
}
