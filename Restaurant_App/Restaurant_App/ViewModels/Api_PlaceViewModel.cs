﻿using Restaurant_App.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant_App.ViewModels
{
    public class Api_PlaceViewModel
    {
        public Place Place { get; set; }
        public List<Place_Order> Orders { get; set; }
    }

    public class Place_Order
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
