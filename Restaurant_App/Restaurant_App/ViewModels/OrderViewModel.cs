﻿using Restaurant_App.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Restaurant_App.ViewModels
{
    public class OrderViewModel
    {
        public Order Order { get; set; }
        public List<ProductViewModel> Products { get; set; }
    }

    public class ProductViewModel : INotifyPropertyChanged
    {
        public Product Product { get; set; }
        public Decimal Total { get; set; }
        public Boolean IsVisible { get; set; }
        public Int32 UnitId { get; set; }
        public Int32 Quantity { get; set; }

        private int qty = 0;

        public int Qty
        {
            get
            {
                return qty;
            }

            set
            {
                if (value != qty)
                {
                    qty = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
