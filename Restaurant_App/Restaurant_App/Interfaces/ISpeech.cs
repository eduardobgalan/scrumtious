﻿namespace Restaurant_App.Interfaces
{
    public interface ISpeech
    {
        void Speak(string text);
    }
}
