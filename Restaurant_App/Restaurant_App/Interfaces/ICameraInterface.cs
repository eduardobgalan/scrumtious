﻿using System.IO;
using System.Threading.Tasks;

namespace Restaurant_App.Interfaces
{
    public interface IPicturePicker
    {
        Task<Stream> GetImageStreamAsync();
    }
}