﻿using Newtonsoft.Json;
using Restaurant_App.Classes;
using Restaurant_App.Interfaces;
using Restaurant_App.Models;
using Restaurant_App.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant_App.Services
{
    public class GeneralService : IRestService
    {
        #region Login module
        public async Task<User> Login(LoginViewModel model)
        {
            HttpClient client = new HttpClient();
            User user = new User();
            var uri = new Uri(string.Format(Credentials.Login));

            try
            {
                var json = JsonConvert.SerializeObject(model);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(uri, content);
                var newContent = await response.Content.ReadAsStringAsync();

                user = JsonConvert.DeserializeObject<User>(newContent);

                if (response.IsSuccessStatusCode)
                    Debug.WriteLine(@"				Usuario loggeado.");
                else
                    user = null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return user;
        }

        public async Task<User> CreateUser(User model)
        {
            HttpClient client = new HttpClient();
            User user = new User();
            var uri = new Uri(string.Format(Credentials.CreateAccount));

            try
            {
                var json = JsonConvert.SerializeObject(model);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(uri, content);
                var newContent = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    user = JsonConvert.DeserializeObject<User>(newContent);
                    user._succ = true; // Marcamos que todo salio bien.
                    Debug.WriteLine(@"				Usuario creado.");
                }
                else
                {
                    var errorMessage = JsonConvert.DeserializeObject<JsonResultModel>(newContent);
                    user._err = errorMessage; // Instanciamos el error en user.
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return user;
        }

        public async Task<bool> RecoverPassword(LoginViewModel model)
        {
            HttpClient client = new HttpClient();
            User user = new User();
            var uri = new Uri(string.Format(Credentials.RecoverPassword));

            try
            {
                var json = JsonConvert.SerializeObject(model);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(uri, content);

                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
                return false;
            }
        }
        #endregion Login module

        #region Places module
        public async Task<Place> ManagePlace(Place model)
        {
            HttpClient client = new HttpClient();
            Models.Place place = new Models.Place();
            var uri = new Uri(string.Format(Credentials.ManagePlace));

            try
            {
                var json = JsonConvert.SerializeObject(model);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(uri, content);
                var newContent = await response.Content.ReadAsStringAsync();

                place = JsonConvert.DeserializeObject<Models.Place>(newContent);

                if (response.IsSuccessStatusCode)
                    Debug.WriteLine(@"				Usuario creado.");
                else
                    place = null;

            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return place;
        }

        public async Task<List<Api_PlaceViewModel>> GetPlaces(int usrId)
        {
            HttpClient client = new HttpClient();
            List<Api_PlaceViewModel> placesList = new List<Api_PlaceViewModel>();
            var uri = new Uri(string.Format(Credentials.GetPlaces, usrId));

            try
            {
                HttpResponseMessage response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    placesList = JsonConvert.DeserializeObject<List<Api_PlaceViewModel>>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return placesList;
        }

        public async Task<List<Category>> GetCategories()
        {
            HttpClient client = new HttpClient();
            List<Category> categoriesList = new List<Category>();
            var uri = new Uri(string.Format(Credentials.GetCategories));

            try
            {
                HttpResponseMessage response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    categoriesList = JsonConvert.DeserializeObject<List<Category>>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return categoriesList;
        }
        #endregion Places module

        #region Orders module
        public async Task<Order> CreateOrder(Order model)
        {
            HttpClient client = new HttpClient();
            Order order = new Order();
            var uri = new Uri(string.Format(Credentials.CreateOder));

            try
            {
                var json = JsonConvert.SerializeObject(model);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(uri, content);

                var newContent = await response.Content.ReadAsStringAsync();

                order = JsonConvert.DeserializeObject<Order>(newContent);

                if (response.IsSuccessStatusCode)
                    Debug.WriteLine(@"				Orden creada.");
                else
                    order = null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return order;
        }

        public async Task<List<Product>> GetProducts()
        {
            HttpClient client = new HttpClient();
            List<Product> productsList = new List<Product>();
            var uri = new Uri(string.Format(Credentials.GetProducts));

            try
            {
                HttpResponseMessage response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    productsList = JsonConvert.DeserializeObject<List<Product>>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return productsList;
        }

        public async Task<Order> GetOrderById(int orderId)
        {
            HttpClient client = new HttpClient();
            Order order = new Order();
            var uri = new Uri(string.Format(Credentials.GetOrderById, orderId));

            try
            {
                HttpResponseMessage response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    order = JsonConvert.DeserializeObject<Order>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return order;
        }

        public async Task<List<Order>> GetOrdersByPlaceId(int placeId)
        {
            HttpClient client = new HttpClient();
            List<Order> ordersList = new List<Order>();
            var uri = new Uri(string.Format(Credentials.GetOrdersByPlaceId, placeId));

            try
            {
                HttpResponseMessage response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    ordersList = JsonConvert.DeserializeObject<List<Order>>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return ordersList;
        }

        public async Task<bool> ChangeOrderStatus(Order order)
        {
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format(Credentials.ChangeOrderStatus));

            try
            {
                var json = JsonConvert.SerializeObject(order);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(uri, content);

                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
                return false;
            }
        }

        public async Task<Message> SetOrderMessage(Message model)
        {
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format(Credentials.SetOrderMessage));

            try
            {
                var json = JsonConvert.SerializeObject(model);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(uri, content);

                if (response.IsSuccessStatusCode)
                {
                    var messagesContent = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<Message>(messagesContent);
                }

                return null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
                return null;
            }
        }

        public async Task<bool> SetOrderReport(Report model)
        {
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format(Credentials.SetOrderReport));

            try
            {
                var json = JsonConvert.SerializeObject(model);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(uri, content);

                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
                return false;
            }
        }

        public async Task<List<Message>> GetOrderMessages(int orderId)
        {
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format(Credentials.GetOrderMessages));
            List<Message> messagesList = new List<Message>();

            var messagesRequest = new MessagesRequest()
            {
                IsReport = false,
                OrderId = orderId
            };

            try
            {
                var json = JsonConvert.SerializeObject(messagesRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(uri, content);

                if (response.IsSuccessStatusCode)
                {
                    var messagesContent = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<List<Message>>(messagesContent);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
                return null;
            }
        }
        #endregion Orders module
    }
}
